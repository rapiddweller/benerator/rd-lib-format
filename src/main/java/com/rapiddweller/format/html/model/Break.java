package com.rapiddweller.format.html.model;

public class Break extends HtmlElement<Break> {

	public Break() {
		super("br", false);
	}

}
